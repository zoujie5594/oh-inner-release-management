# oh-inner-release-management

Release Manager的主要工作职责是在社区协调各SIG的Maintainer、QA等各个团队，完成OpenHarmony社区版本的发布工作。主要职责如下：<br>
	• 规划和计划OpenHarmony版本的发行时间表 <br>
	• 在开发/测试周期中跟踪（更新updates或功能feature）的开发状态 <br>
	• 组织开发资源有序参与社区开发活动，交付社区版本和补丁 <br>
	• 组织其它社区相关的开发活动 <br>
